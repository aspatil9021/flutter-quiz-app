import 'package:quizzler_2/question.dart';

class QuizLogic {
  int _mQuestionNumber = 0;
  List<Question> _mQuestionBank = [
    Question(questionText: 'India won the world cup 3 times.', questionAnswer: false),
    Question(questionText: 'M S Dhoni is the most successful captain of Indian team.', questionAnswer: true),
    Question(questionText: 'Pakistan won the first world cup 1975.', questionAnswer: false),
    Question(questionText: 'India won the 2017 champions trophy.', questionAnswer: true),
    Question(questionText: 'Rohit Sharma has done two double centuries in ODI.', questionAnswer: true),
    Question(questionText: 'Indian bowler Jasprit Bumrah is ODI\'s most successful bowler.', questionAnswer: true),
    Question(questionText: 'Virat Kohli won the maximum sixes award for the IPL 2008 season?', questionAnswer: false),
    Question(questionText: 'Indian team is known as team incredible.', questionAnswer: true),
    Question(questionText: 'India is no 1 test team.', questionAnswer: true),
    Question(questionText: 'Indian bowler Kuldeep Yadap is world\'s only left handed leg spinner.', questionAnswer: true),
    Question(questionText: 'Australia played its first Test against England as a sovereign nation.', questionAnswer: false),
    Question(questionText: 'Palwankar Baloo was India’s first Test captain.', questionAnswer: false),
    Question(questionText: 'The colonisers did nothing to encourage the Parsis in playing cricket.', questionAnswer: true),
    Question(questionText: 'India joined the world of Test cricket before independence.', questionAnswer: true)
  ];

  void nextQuestion() {
    if (_mQuestionNumber < _mQuestionBank.length - 1) {
      _mQuestionNumber += 1;
    }
  }

  String getQuestionText() {
    return _mQuestionBank[_mQuestionNumber].questionText;
  }

  bool getQuestionAnswer() {
    return _mQuestionBank[_mQuestionNumber].questionAnswer;
  }

  bool isLastQuestion() {
    if(_mQuestionNumber == _mQuestionBank.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  void resetCounter() {
    _mQuestionNumber = 0;
  }

  int getTotalQuestionCount() {
    return _mQuestionBank.length;
  }
}
