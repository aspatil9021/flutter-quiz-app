import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:quizzler_2/quiz_logic.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

QuizLogic _quizLogic = QuizLogic();

void main() => runApp(Quizzler());

class Quizzler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: QuizPage(),
          ),
        ),
      ),
    );
  }
}

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Widget> _mScoreKeeper = [];
  int _correctAnswerCount = 0;

  void _emptyScoreKeeper() {
    _mScoreKeeper.clear();
  }

  void showAlertDialog() {
    String _result = "";
    if (_correctAnswerCount >= (_quizLogic.getTotalQuestionCount() / 2)) {
      _result = 'Pass';
    } else {
      _result = 'Fail';
    }

    Alert(
      context: context,
      type: AlertType.success,
      title: "Finished !",
      desc:
          "You have reached the end of the quiz. \n\n Your Score : $_result \n Total Questions : ${_quizLogic.getTotalQuestionCount()} \n Correct Answers : $_correctAnswerCount \n Incorrect Answers : ${_quizLogic.getTotalQuestionCount() - _correctAnswerCount}",
      buttons: [
        DialogButton(
          padding: EdgeInsets.all(10.0),
          child: Text(
            "CANCEL",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
            setState(() {
              _quizLogic.resetCounter();
              _emptyScoreKeeper();
              _correctAnswerCount = 0;
            });
          },
          height: 44.0,
        )
      ],
    ).show();
  }

  void checkUserAnswer(bool userAnswer) {
    bool correctAnswer = _quizLogic.getQuestionAnswer();

    setState(() {
      if (_quizLogic.isLastQuestion()) {
        showAlertDialog();
      }

      if (correctAnswer == userAnswer) {
        _correctAnswerCount += 1;
        _mScoreKeeper.add(Icon(Icons.check, color: Colors.green));
      } else {
        _mScoreKeeper.add(Icon(Icons.close, color: Colors.red));
      }
      _quizLogic.nextQuestion();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                _quizLogic.getQuestionText(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.green)),
              child: Text(
                'TRUE',
                style: TextStyle(color: Colors.white, fontSize: 25.0),
              ),
              onPressed: () {
                checkUserAnswer(true);
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.red)),
              child: Text(
                'FALSE',
                style: TextStyle(color: Colors.white, fontSize: 25.0),
              ),
              onPressed: () {
                checkUserAnswer(false);
              },
            ),
          ),
        ),
        Row(children: _mScoreKeeper)
      ],
    );
  }
}
